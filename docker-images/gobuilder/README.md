###### Manual for Building image

> Download golang tar from <https://storage.googleapis.com/golang/go1.8.3.linux-amd64.tar.gz>

> Prepare the golang dependencies and make a tar ball, which includes all third party's libs in $GOPATH/src

> Ensure that the docker file and tar balls is in the same dir.

> Run docker build -t "YourRegistryHost"/ffs/gobuilder:1.8.3 .

###### HowTo Use The Image

*The following steps is recording when building farm_beauty, the logic is same for other services*

0. git clone / git pull the code repo and git checkout to the desired branch.

1. Choose a service you want to build with golang. e.g. farm_beauty

2. Ensure you are using the latest image:

    `docker pull "ImageName:tag`

3. Run build:

    `docker run --rm -it -v "PathToYourRepo":"/go/.../familyfarm-server-midware" "ImageName:tag" bash -c "/go/bin/run_build.sh farm_beauty"`

4. Verify the logging info and check whenever an error alerts.

5. If all succeed, the target package(binary with configs) will be found at "PathToYourRepo"/farm_beauty

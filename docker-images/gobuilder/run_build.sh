#!/bin/bash

success() { printf "\e[32m✔ %s\e[0m\n" "$@"
}
error() { printf "\e[31m✖ %s\e[0m\n" "$@"
}
warn() { printf "\e[33m➜ %s\e[0m\n" "$@"
}

REPO_ROOT=/go/src/bitbucket.org/funplus/familyfarm-server-midware
PACKAGE_ROOT=/go/src/bitbucket.org/funplus/familyfarm-server-midware/packages


if [ $# -ne 1 ]; then
	warn " Usage: $0 <service-name>" && exit 1
fi


# The src path is generated according to repo structure.
# Always remember to modify this script if your repo structure changes.
current_svc=$1
SRC_PATH=${REPO_ROOT}/${current_svc}/cmd/${current_svc}
if [ ! -d ${SRC_PATH} ]; then
	error "[ERROR] Illegal golang service specified (src folder dosen't exist!)" && exit 1
else
	success "[INFO] Go source code dir for \"${current_svc}\" checked"
fi


# At present we hard-coded the dir which stores the built golang binary.
PACKAGE_PATH=${PACKAGE_ROOT}/${current_svc}
DEST_PACKAGE=${PACKAGE_PATH}/${current_svc}
if [ -f ${DEST_PACKAGE} ]; then
	warn "[WARNING] \"${DEST_PACKAGE}\" already exists and will be overwritten by gobuilder"
fi


# Build the binary
mkdir -p ${PACKAGE_PATH}
[ $? != 0 ] && error "[ERROR] Create dir ${PACKAGE_PATH} failed" && exit 1
success "[INFO] Successfully created package path"
cd ${SRC_PATH}
[ $? != 0 ] && error "[ERROR] Failed to change to dir ${SRC_PATH}" && exit 1
success "[INFO] Switched to source code directory (working dir)"
go get
[ $? != 0 ] && error "[ERROR] Godep failed" && exit 1
success "[INFO] Successfully installed go dependencies using go get"
go build -o ${DEST_PACKAGE}
[ $? != 0 ] && error "[ERROR] Go build failed" && exit 1
success "[INFO] Successfully build service \"${current_svc}\""

# Package related configs with the binary.
REPO_SHARED_CONFIG_PATH=${REPO_ROOT}/ops/etc
REPO_SERVICE_CONDIF_PATH=${REPO_ROOT}/${current_svc}/etc
PACKAGE_SHARED_CONFIG_PATH=${PACKAGE_PATH}/ops/etc
PACKAGE_SERVICE_CONDIF_PATH=${PACKAGE_PATH}/ops/${current_svc}
if [ -d ${REPO_SHARED_CONFIG_PATH} ]; then
	mkdir -p ${PACKAGE_SHARED_CONFIG_PATH}
	[ $? != 0 ] && error "[ERROR] Create shared config dir ${PACKAGE_SHARED_CONFIG_PATH} failed" && exit 1
	cp -r ${REPO_SHARED_CONFIG_PATH}/* ${PACKAGE_SHARED_CONFIG_PATH}
	[ $? != 0 ] && error "[ERROR] Copy shared configs from \"${REPO_SHARED_CONFIG_PATH}\" to \"${PACKAGE_SHARED_CONFIG_PATH}\" failed" && exit 1
	success "[INFO] Successfully packaged common configs"
fi
if [ -d ${REPO_SERVICE_CONDIF_PATH} ]; then
	mkdir -p ${PACKAGE_SERVICE_CONDIF_PATH}
	[ $? != 0 ] && error "[ERROR] Create service config dir ${PACKAGE_SERVICE_CONDIF_PATH} failed" && exit 1
	cp -r ${REPO_SERVICE_CONDIF_PATH}/* ${PACKAGE_SERVICE_CONDIF_PATH}
	[ $? != 0 ] && error "[ERROR] Copy service configs from \"${REPO_SERVICE_CONDIF_PATH}\" to \"${PACKAGE_SERVICE_CONDIF_PATH}\" failed" && exit 1
	success "[INFO] Successfully packaged configs for \"${current_svc}\""
fi


# Package supervisor configs with the binary.
REPO_SUPERVISOR_CONDIF_PATH=${REPO_ROOT}/supervisor/prod/${current_svc}
PACKAGE_SUPERVISOR_CONDIF_PATH=${PACKAGE_PATH}/supervisor/${current_svc}
if [ -d ${REPO_SUPERVISOR_CONDIF_PATH} ]; then
	mkdir -p ${PACKAGE_SUPERVISOR_CONDIF_PATH}
	[ $? != 0 ] && error "[ERROR] Create supervisor config dir ${PACKAGE_SUPERVISOR_CONDIF_PATH} failed" && exit 1
	cp -r ${REPO_SUPERVISOR_CONDIF_PATH}/* ${PACKAGE_SUPERVISOR_CONDIF_PATH}
	[ $? != 0 ] && error "copy supervisor configs from \"${REPO_SUPERVISOR_CONDIF_PATH}\" to \"${PACKAGE_SUPERVISOR_CONDIF_PATH}\" failed" && exit 1
	success "[INFO] Successfully packaged supervisor config for \"${current_svc}\""
fi

echo && success "Build and package \"${current_svc}\" Successfully"
exit 0


package main

import (
	"fmt"
	"time"
)

func main() {
	cnt := 0
	for {
		cnt++
		cnt %= 10
		fmt.Println(cnt,": hello world[version 1.0.7]")
		time.Sleep(time.Second)
	}
}
